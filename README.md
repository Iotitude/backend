## Backend automation scripts and guides

**[Check the wiki for more guides and help](https://gitlab.com/Iotitude/backend/wikis/home)**

This repo contains various scripts to automate the installation of various
backend servers.

Scripts included:

+ Custom CentOS Kickstart Usb creation tool
+ Post install script that allows you to install
  - Cassandra
  - Docker
  - MariaDB


## Bootable USB drive preparation

**Warning! Please make sure that you understand every step in this guide
before actually executing any commands. Careless execution may lead into
loss of data!**

### Sources

[pauljeff's gist](https://gist.github.com/pauljeff/4b9ad551cb6c35870d7c)

[Kickstart options](https://www.centos.org/docs/5/html/Installation_Guide-en-US/s1-kickstart2-options.html)

[Kickstart guide at CentOS docs](https://www.centos.org/docs/5/html/Installation_Guide-en-US/ch-kickstart2.html)

### Required packages

The following packages are required:

* dosfstools for creating a fat32 partition
* syslinux for the bootloader

Also we need to download an iso image of distribution of choice.
This guide is written for CentOS 7 minimal that can be found
[here](http://buildlogs.centos.org/rolling/7/isos/x86_64/CentOS-7-x86_64-Minimal.iso)
. Other distributions **might** work.

### Determining the USB key's name

We need to find out the name that our OS assigns to our USB key,
so we don't accidentally destroy data on our other hard drives.

With our USB key **unplugged**, we run `lsblk` to list the attached
block devices

Example output of `lsblk`:

```
$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sdb      8:16   0 931,5G  0 disk 
sr0     11:0    1  1024M  0 rom  
sda      8:0    0 931,5G  0 disk 
├─sda2   8:2    0     1K  0 part 
├─sda5   8:5    0  15,9G  0 part [SWAP]
└─sda1   8:1    0 915,6G  0 part /
```

Now let's attach our USB key to a port on our computer and run `lsblk` again

Example output after attaching the USB key:

```
$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sdb      8:16   0 931,5G  0 disk 
sr0     11:0    1  1024M  0 rom  
sdX      8:32   1   7,3G  0 disk 
sda      8:0    0 931,5G  0 disk 
├─sda2   8:2    0     1K  0 part 
├─sda5   8:5    0  15,9G  0 part [SWAP]
└─sda1   8:1    0 915,6G  0 part /
```

A new device named *sdX* (actually the name is probably sdb or sdc, depending on
the amount of hard drives on our computer, in my case the name is sdc, because I
already have two hard drives attached to my computer) appeared on the list, this
the name of our USB key.

In the future you should substitute all instances of *sdX* with the actual name
of your device determined here.

At this point you should edit *mount.sh* to point to your USB key.

### Partitioning the USB key

Now that we have determined the name of our USB key, we need to partition it,
please note that during partitioning, any data on the key **will be erased**
so make sure that there's no important data on the key before continuing.

To partition the key, enter the following commands

```
sudo fdisk /dev/sdX
n (creates a partition, accept defaults for type, number, and first sector)
+250M (defines the size of the partition to be 250MiB)
t
c (changes partition type to W95 FAT32 (LBA))
a (makes the partition bootable)
n (creates a partition, accept defaults for type, number, first sector and size)
w (writes changes to the device)
q (exit fdisk)
```

Next we format the created partitions

```
sudo mkfs -t vfat -n "BOOT" /dev/sdX1
sudo mkfs -L "DATA" /dev/sdX2
```

Next we write the Master boot record data to the device

```
sudo dd conv=notrunc bs=440 count=1 if=/usr/share/syslinux/mbr.bin of=/dev/sdX
```

The location of mbr.bin depends on distribution, on Ubuntu, the path is
/usr/lib/syslinux/mbr/mbr.bin

Now we'll install syslinux to the first partition

```
sudo syslinux /dev/sdX1
```

### Copying the files to the USB key

At this point you should have successfully partitioned your USB key and output
of `lsblk` should look something like this:

```
$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sdb      8:16   0 931,5G  0 disk 
sr0     11:0    1  1024M  0 rom  
sdX      8:32   1   7,3G  0 disk 
├─sdX2   8:34   1     7G  0 part /media/username/DATA <-- new!
└─sdX1   8:33   1   250M  0 part /media/username/BOOT1 <-- new!
sda      8:0    0 931,5G  0 disk 
├─sda2   8:2    0     1K  0 part 
├─sda5   8:5    0  15,9G  0 part [SWAP]
└─sda1   8:1    0 915,6G  0 part /
```

You should also have the iso file of your distribution at hand.

To begin we'll mount the partitions and the iso

```
mkdir BOOT && sudo mount /dev/sdX1 BOOT
mkdir DATA && sudo mount /dev/sdX2 DATA
mkdir DVD && sudo mount /path/to/your/distro.iso DVD
```

Copy isolinux from the DVD to `BOOT`

```
sudo cp DVD/isolinux/* BOOT
```

rename `isolinux.cfg` to `syslinux.cfg`

```
sudo mv BOOT/isolinux.cfg BOOT/syslinux.cfg
```

Next we copy the `ks.cfg` provided to `BOOT`
(You should customize your ks.cfg to suit your needs, also the file
is very hardware specific, what works on our machines might not work
on yours)

```
cp /path/to/ks.cfg BOOT
```

Also we need to copy our iso file to `DATA`

```
cp /path/to/your/distro.iso DATA
```

The final structure of `BOOT` and `DATA` should look something like this:

```
BOOT/
├── boot.cat
├── boot.msg
├── initrd.img
├── ks.cfg
├── ldlinux.sys
├── memtest
├── splash.png
├── syslinux.cfg
├── upgrade.img
├── vesamenu.c32
└── vmlinuz
DATA/
└── distro.iso
```


### Editing the syslinux.cfg

Now we need to edit `syslinux.cfg` so it points to our iso and kickstart files.
Here is the default *Install CentOS 7* entry from the minimal iso I used.

```
label linux                                                               
  menu label ^Install CentOS 7      
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20x86_64 quiet
```

the `append` line is changed to:

```
append initrd=initrd.img inst.stage2=hd::LABEL=DATA:/ ks=hd:LABEL=BOOT:/ks.cfg
```
