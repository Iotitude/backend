#! /bin/sh

# TODO:
#
# + Configure Cassandra after installation
#   - Configure ports
#   - Run the cql file provided by Kaa
#

prompt='Would you like to install additional software?
1. Docker
2. Cassandra
3. MariaDB
4. No thanks
> '

install_docker() {
    echo "Installing Docker!"
    curl https://releases.rancher.com/install-docker/1.13.sh | sh
    chkconfig docker on
    echo "Done!"
}

install_cassandra() {
    echo "Installing Cassandra!"
    yum install java-1.8.0-openjdk-devel -y
    repo="[datastax-ddc]
name = DataStax Repo for Apache Cassandra
baseurl = http://rpm.datastax.com/datastax-ddc/3.6
enabled = 1
gpgcheck = 0"
   echo "$repo" > /etc/yum.repos.d/datastax.repo
   yum install jna -y
   yum install datastax-ddc -y
   service cassandra start
   chkconfig cassandra on
   echo "Done!"
}

install_mariadb() {
    echo "Installing MariaDB!"
    repo="[mariadb]
name = MariaDB
baseurl = https://downloads.mariadb.com/MariaDB/mariadb-5.5.53/yum/centos7-amd64/
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1"
    echo "$repo" > /etc/yum.repos.d/MariaDB.repo
    # yum -y update
    # service mysqld stop
    # yum -y remove mysql-server mysql
    yum install -y MariaDB-Galera-server MariaDB-client galera
    service mysql start
    /usr/bin/mysql_secure_installation
    echo "CREATE USER 'sqladmin'@'localhost' IDENTIFIED BY 'admin';" >> kaa.sql #echo into file
    echo "GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'localhost' WITH GRANT OPTION;" >> kaa.sql #echo into file
    echo "GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'%' WITH GRANT OPTION;" >> kaa.sql #echo into file
    echo "FLUSH PRIVILEGES;" >> kaa.sql #echo into file
    echo "CREATE DATABASE kaa CHARACTER SET utf8 COLLATE utf8_general_ci;" >> kaa.sql #echo into file
    echo "Please login to mysql with the root password you specified earlier"
    mysql --user=root -p < kaa.sql  # << password for maria db root
    rm kaa.sql
    echo "Done!"
}

systemctl disable firewalld

while true; do
    printf "%s" "$prompt"
    read choice
    case $choice in
        [1]* ) install_docker; break;;
        [2]* ) install_cassandra; break;;
        [3]* ) install_mariadb; break;;
        [4]* ) echo "Ok, bye!";;
        * ) echo "Invalid choice $choice";;
    esac
done
exit
