#! /bin/sh
# Attention! Change sdX1 and sdX2 to point to your USB key's name
# e.g. to sdc1 and sdc2
mkdir -p BOOT && mount /dev/sdX1 BOOT
mkdir -p DATA && mount /dev/sdX2 DATA
echo "Done"
